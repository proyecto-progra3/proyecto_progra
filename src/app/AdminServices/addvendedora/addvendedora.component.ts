import { Component, OnInit } from '@angular/core';
import {ProyectopService} from '../../service/proyectop.service';
import {FormBuilder, FormGroup, FormArray, FormControl, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router'
import { AuthenticationService } from 'src/app/service/authentication.service';

@Component({
  selector: 'app-addvendedora',
  templateUrl: './addvendedora.component.html',
  styleUrls: ['./addvendedora.component.css']
})
export class AddvendedoraComponent implements OnInit {
  
  vendedora:any = {
    telefono:[],
    pedidos:[]
  };
  miFormulario:FormGroup;
  edit: boolean = false;
  password="";
  password1="";

  constructor(public loginservice: AuthenticationService, private vendedoraService:ProyectopService, private fb:FormBuilder, private route:Router,private ActiveRoute:ActivatedRoute) { }

  ngOnInit() {
  

    this.miFormulario=this.fb.group({
       telefono: this.fb.array([this.fb.group({numero: [''],idtelefonos:['']})])
    });
    
    const params= this.ActiveRoute.snapshot.params;
    if (params.id) {
      this.vendedoraService.getVendedora(params.id)
        .subscribe(
          res => {
            console.log(res);
            this.vendedora = res;
            this.password=this.vendedora.password;
            this.setTelefonos(this.vendedora);
            
            this.edit = true;
          },
          err => console.log(err)
        )
    }
  }
passwordMatch(){
  if(this.isValidPass()){
    if (this.password===this.password1)
    this.saveVendedora();
  else
  alert("Las contraseñas no coinciden");
  } else
  alert("Las contraseña debe contener al menos: 1 Mayúsucula, 1 Minúscula, 1 Dígito");
  
}
isValidPass(){
  var regex=new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{8,})");
  var valid=regex.test(this.password);
  if(valid==true)
  return true;
  else
  return false;
}
  saveVendedora(){
    this.vendedora.telefono=this.miFormulario.value.telefono;
    this.vendedora.password=this.password;
    this.vendedoraService.saveVendedora(this.vendedora).subscribe(
      res=>{
        this.usuarioCreado(res);
        this.route.navigate(['/admin-dashboard/vendedoras']);
      },
      err=> console.error(err)
    )
  }
  updateVendedora(){
    if(this.isValidPass()){
    if (this.password===this.password1)
    this.upPasswordMatch();
    else
  alert("Las contraseñas no coinciden");}
  else
  alert("Las contraseña debe contener al menos: 1 Mayúsucula, 1 Minúscula, 1 Dígito");
  }

  upPasswordMatch(){
    this.vendedora.telefono=this.miFormulario.value.telefono;
    this.vendedora.password=this.password;
    this.vendedoraService.updateVendedora(this.vendedora.codigo, this.vendedora).subscribe(
      res=>{
        alert("Vendedora actualizada con ID: " + this.vendedora.codigo);
        this.route.navigate(['/admin-dashboard/vendedoras']);
      },
      err=> console.error(err)
    )
  }

  usuarioCreado(vendedora:any){
    alert("Vendedora creada con ID: " + vendedora.codigo);
  }

  get getTelefonos(){
    return this.miFormulario.get('telefono') as FormArray;
  }

  setTelefonos(vendedora:any){
    const control = <FormArray>this.miFormulario.controls['telefono'];
    control.removeAt(0);
     for(var i=0;i<vendedora.telefono.length;i++){
      control.push(this.fb.group({numero:vendedora.telefono[i].numero, idtelefonos:vendedora.telefono[i].idtelefonos}));
    }
  }

  addTelefono(){
    const control = <FormArray>this.miFormulario.controls['telefono'];
    control.push(this.fb.group({numero:[], idtelefonos:[]}));
  }

  removeTelefono(index:number){
    const control = <FormArray>this.miFormulario.controls['telefono'];
    var id=this.miFormulario.value.telefono[index].numero;
    var idte=this.miFormulario.value.telefono[index].idtelefonos;
    console.log(idte);
    if (id!=null){
      this.vendedoraService.deleteTelefono(idte).subscribe(
        res => {
          console.log(res);
          },
          err => console.error(err)
      )}
    control.removeAt(index);
  }
}
//*if (id!=null){
//  this.vendedoraService.deleteTelefono(id.numero)
//  .subscribe(
//    res => {
//      console.log(res);
//    },
//    err => console.error(err)
 // )
//  alert("Telefono borrado con ID: " + index);
//}